variable "project_id" {
  description = "Google Project ID to be used"
}
variable "secret" {
  description = "secret.json para ler"
}
variable "image_name" {
  description = "Nome da imagem"
}

provider "google" {
  credentials = var.secret
  project     = var.project_id 
  region      = "us-central1"
}

resource "google_compute_instance" "ubuntu-bruno-emanuel-nginx-19" {
  name = var.image_name
  machine_type = "n1-standard-1"
  zone = "us-central1-a"

  boot_disk {
    initialize_params {
      image = var.image_name
    }
  }
  network_interface {
    network = "default"
  }
}
